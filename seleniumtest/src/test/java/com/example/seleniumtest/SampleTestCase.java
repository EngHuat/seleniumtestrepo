package com.example.seleniumtest;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
public class SampleTestCase {

	static WebDriver driver;
	String pagetitle;

	@BeforeClass
	public static void setup() throws Exception {
		// Initializing HtmlUnitDriver.
		// driver = new HtmlUnitDriver();
		// driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//
		// // Opening URL In HtmlUnitDriver.
		// driver.get("http://www.google.com");

		// File file = new File("src/test/java/phantomjs.exe");
		// System.setProperty("phantomjs.binary.path", file.getAbsolutePath());
		// driver = new PhantomJSDriver();
		// String baseUrl = "http://www.google.com";
		// driver.get(baseUrl);
		
		final ChromeOptions chromeOptions = new ChromeOptions();
		System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver"); // Change driver to chromedriver.exe if testing on windows
		System.setProperty("webdriver.chrome.logfile", "./chromedriver.log");
		System.setProperty("webdriver.chrome.verboseLogging", "true");
		chromeOptions.addArguments("--headless");
		chromeOptions.addArguments("--disable-gpu");
		chromeOptions.addArguments("--window-size=1366,768");
		chromeOptions.addArguments("--no-sandbox");
		driver = new ChromeDriver(chromeOptions);
		driver.get("http://www.google.com");
	}

	@AfterClass
	public static void tearDown() throws Exception {
		// Closing HtmlUnitDriver.
		driver.quit();
	}

	@Test
	public void test() {
		// Get and print page title before search.
		pagetitle = driver.getTitle();
		System.out.println("Page title before search : " + pagetitle);

		// Search with Hello World on google.
		WebElement Searchbox = driver.findElement(By.xpath("//input[@name='q']"));
		Searchbox.sendKeys("Hello World");
		Searchbox.submit();

		// Get and print page title after search.
		pagetitle = driver.getTitle();
		Assert.assertEquals(pagetitle, "Google");
		System.out.println("Page title after search : " + pagetitle);
		// Get list of search result strings.
		List<WebElement> allSearchResults = driver.findElements(By.cssSelector("ol li h3>a"));

		// Iterate the above list to get all the search titles & links from that page.
		for (WebElement eachResult : allSearchResults) {
			System.out.println("Title : " + eachResult.getText() + ", Link : " + eachResult.getAttribute("href"));
		}
	}

}
